setwd("C:/Users/lucas/Desktop/Python/sscpraktikum")
rm(list=ls())
library(httpgd)
library(data.table)
source("minorticks.r")

par(mfrow= c(1,1))
calibration_factor <- sqrt(2)/1.273
baseres <- 10000 #10kΩ
#Code AuAu photcurrent vs bias V (U=RI -> I=(U*calibration factor)/R(10kΩ)
r1px<- c(0,1,20,40,60,80,100) # run 1 postiv x
r1py<- (c(0,1.178,9.3,12.9,19.7,26.8,33.3)*calibration_factor)/baseres

r1nx <- c(-20,-40,-60,-80,-100) # run 1 negative x
r1ny <- (c(9.04,16,9.35,10.64,13.40)*calibration_factor)/baseres

r1ax <- c(-100,-80,-60, -40,-20,0,1,20,40,60,80,100) # run1 all x 
r1ay <- (c(13.4, 10.64, 9.35, 16, 9.04, 0, 1.178, 9.3, 12.9, 19.7, 26.8, 33.3)*calibration_factor)/baseres

r2x <- c(-20,-40,-60,-80,-100) # run 2 x
r2y <- (c(14.21, 22.2, 17.06, 11.32, 14.45)*calibration_factor)/baseres
plot(r1px, r1py, xlab= "Bias Voltage [V]", ylab="Photocurrent [mA]", yaxt="n", xlim=c(-100,100))
points(r1nx,r1ny, col = "blue") # negative bias voltage measurement 1
points(r2x,r2y, col = "green") # negative bias voltage measurement 2
#better readable y axis ticks
y1 <- c(0, 0.001, 0.002, 0.003)
y_labels <- c(0, 1, 2, 3)
axis(2, at = y1, labels = y_labels)
#fit with hecht equation
hecht1 <- nls(r1py~A*(r1px/B)*(1-exp(B/r1px)), start = list(A=-1, B=-100), data = data.frame(r1px, r1py))
print(summary(hecht1))
newx <- seq(0,100,0.001)
newy <- predict(hecht1, list(r1px = newx))
matlines(newx,newy)
dev.copy2pdf(file = "plots/photoconductivity/AuAu_hecht_fail.pdf", height=10, width=15)



#AuBi used for hecht fit
bix <- c(100, 80, 60, 40, 20, 10, 5) # absolute voltage values
biy <- (c(29.17, 21.71, 15.05, 9.35, 4.91, 4.52, 4)*calibration_factor)/baseres
bixfit <- c(100, 80, 60, 40, 20, 10, 5, 110,120,130,140,150,160,170,180,190, 200, 210, 220, 230, 240) # 110-140 expraploation for fit
biyfit <- (c(29.17, 21.71, 15.05, 9.35, 4.91, 4.52, 4, 29.2, 29.18,29.21,29.19,29.2,29.19,29.2,29.18,29.18,29.2,29.19,29.2,29.18,29.18)*calibration_factor)/baseres
extrax <- c(110,120,130,140,150,160,170,180,190, 200, 210, 220, 230, 240) # 110-140 expraploation for fit
extray <- (c(29.2, 29.18,29.21,29.19,29.2,29.19,29.2,29.18,29.18,29.2,29.19,29.2,29.18,29.18)*calibration_factor)/baseres

plot(bix,biy, 
xlim=c(0,250),
yaxt="n",
xlab="Voltage [V]",
ylab = "Photocurrent [mA]")
points(extrax,extray, col="red")
y1 <- c(0, 0.0005,0.001,0.0015,0.002,0.0025,0.003)
y_labels <- c(0, 0.5, 1,1.5, 2, 2.5,3)
axis(2, at = y1, labels = y_labels)

hecht2 <- nls(
    biyfit~A*(bixfit/B)*(1-exp(-B/bixfit)), 
    start = list(A=-1, B=-1), 
    data = data.frame(bixfit, biyfit)
)
print(summary(hecht2))
B <- 1.302e+02
d <- 0.211
ut <- (d*d)/B
print(paste(ut, "cm^2 / V"))
newx2 <- seq(0,250,0.001)
newy2 <- predict(hecht2, list(bixfit = newx2))
matlines(newx2,newy2)
dev.copy2pdf(file = "plots/photoconductivity/AuBi_optimistic_data_extrapolation_upper_limit_ut.pdf", height=10, width=15)

#molfilm
filmpx <- c(0, 0.25, 0.5, 0.75, 1, 1.25, 1.5)
filmpy <- (c(2.75, 2.48, 3.38, 6.3, 10.7, 15.7, 21.27)*calibration_factor)/baseres
plot(filmpx,filmpy,
    yaxt="n",
    xlab="Voltage [V]",
    ylab = "Photocurrent [mA]"
)
y1 <- c(0, 0.0005, 0.001, 0.0015, 0.002)
y_labels <- c(0, 0.5, 1 , 1.5, 2)
axis(2, at = y1, labels = y_labels)

dev.copy2pdf(file = "plots/photoconductivity/film_raw_data_no_hecht.pdf", height=10, width=15)


#######################################################################################
#Responsitivity
C <- 1.63e4
Aref <- 0.785 #cm^2
#AuAu
A_au_au <- pi*0.4*0.4 #A sample AuAu cm^2
A_au_bi <- pi*0.4*0.4 #A sample AuBi cm^2
A_film <- 1 # square 1x1 cm

file <- "data/Photoconductivity/AuAu/photocurrentVSwavelength 400nm 600nm 5nm 10V bias_100kOhm"
data <- read.csv(file, sep = "")
names(data) <- c("wavelength", "signal", "phase", "ref", "sth1","sth2", "sth3")

wl <- data[,1]
Sa <- data[,2]
Sph <- data[,3]
Sref <- data[,4]
R <- (C * Sa * Aref * cos(Sph)) / (baseres * Sref * A_au_au)

plot(wl, R,
        xlab = "Wavelength [nm]",
        ylab = "Responsivity [A/W]",
        pch = 16
)
dev.copy2pdf(file = "plots/photoconductivity/AuAu_reponsivity.pdf", height=10, width=15)

#theoretical stuff for AuAu
wl_um <- wl / 1000 # nm wavelength into photon wavelength unit um
R_th <- wl / (1.24) # multiply by unit A/W or divide W/A see script page 44
n_eqe <- R / R_th
#wl(nm) vs Rth
plot(wl, R_th,
        xlab = "Wavelength [nm]",
        ylab = "Theor. Responsivity [A/W]",
        pch = 16
)
dev.copy2pdf(file = "plots/photoconductivity/AuAu_theor_reponsivity_vs_wl(nm).pdf", height=10, width=15)
#wl(um) vs Rth
plot(wl_um, R_th,
        xlab = expression("Wavelength"~"["*mu*m*"]"),
        ylab = "Theor. Responsivity [A/W]",
        pch = 16
)
dev.copy2pdf(file = "plots/photoconductivity/AuAu_theor_reponsivity_vs_wl(um).pdf", height=10, width=15)
#wl(nm) vs neqe
plot(wl, n_eqe,
        xlab = "Wavelength [nm]",
        ylab = expression(eta[E * Q * E]),
        pch = 16
)
dev.copy2pdf(file = "plots/photoconductivity/AuAu_neqe_vs_wl(nm).pdf", height=10, width=15)
#wl(um) vs neqe
plot(wl_um, n_eqe,
        xlab = expression("Wavelength"~"["*mu*m*"]"),
        ylab = expression(eta[E * Q * E]),
        pch = 16
)
dev.copy2pdf(file = "plots/photoconductivity/AuAu_neqe_vs_wl(um).pdf", height=10, width=15)

#AuBi
file <- "data/Photoconductivity/AuBi/Photocurrent_vs_wavelength_bias-100V"
data <- read.csv(file, sep = "")
names(data) <- c("wavelength", "signal", "phase", "ref", "sth1","sth2", "sth3")

wl <- data[,1]
Sa <- data[,2]
Sph <- data[,3]
Sref <- data[,4]
R <- (C * Sa * Aref * cos(Sph)) / (baseres * Sref * A_au_bi)

plot(wl, R,
        xlab = "Wavelength [nm]",
        ylab = "Responsivity [A/W]",
        pch = 16
)
dev.copy2pdf(file = "plots/photoconductivity/AuBi_reponsivity.pdf", height=10, width=15)

#theoretical stuff for AuBi
wl_um <- wl / 1000 # nm wavelength into photon wavelength unit um
R_th <- wl / (1.24) # multiply by unit A/W or divide W/A see script page 44
n_eqe <- R / R_th
#wl(nm) vs Rth
plot(wl, R_th,
        xlab = "Wavelength [nm]",
        ylab = "Theor. Responsivity [A/W]",
        pch = 16
)
dev.copy2pdf(file = "plots/photoconductivity/AuBi_theor_reponsivity_vs_wl(nm).pdf", height=10, width=15)
#wl(um) vs Rth
plot(wl_um, R_th,
        xlab = expression("Wavelength"~"["*mu*m*"]"),
        ylab = "Theor. Responsivity [A/W]",
        pch = 16
)
dev.copy2pdf(file = "plots/photoconductivity/AuBi_theor_reponsivity_vs_wl(um).pdf", height=10, width=15)
#wl(nm) vs neqe
plot(wl, n_eqe,
        xlab = "Wavelength [nm]",
        ylab = expression(eta[E * Q * E]),
        pch = 16
)
dev.copy2pdf(file = "plots/photoconductivity/AuBi_neqe_vs_wl(nm).pdf", height=10, width=15)
#wl(um) vs neqe
plot(wl_um, n_eqe,
        xlab = expression("Wavelength"~"["*mu*m*"]"),
        ylab = expression(eta[E * Q * E]),
        pch = 16
)
dev.copy2pdf(file = "plots/photoconductivity/AuBi_neqe_vs_wl(um).pdf", height=10, width=15)



#film
file <- "data/Photoconductivity/MolFilm/Photocurrent_vs_wavelength_bias0V"
data <- read.csv(file, sep = "")
names(data) <- c("wavelength", "signal", "phase", "ref", "sth1","sth2", "sth3")

wl <- data[,1]
Sa <- data[,2]
Sph <- data[,3]
Sref <- data[,4]
R <- (C * Sa * Aref * cos(Sph)) / (baseres * Sref * A_film)

plot(wl, R,
        xlab = "Wavelength [nm]",
        ylab = "Responsivity [A/W]",
        pch = 16
)
dev.copy2pdf(file = "plots/photoconductivity/MolFilm_reponsivity.pdf", height=10, width=15)

#theoretical stuff for Molfilm
wl_um <- wl / 1000 # nm wavelength into photon wavelength unit um
R_th <- wl / (1.24) # multiply by unit A/W or divide W/A see script page 44
n_eqe <- R / R_th
#wl(nm) vs Rth
plot(wl, R_th,
        xlab = "Wavelength [nm]",
        ylab = "Theor. Responsivity [A/W]",
        pch = 16
)
dev.copy2pdf(file = "plots/photoconductivity/molfilm_theor_reponsivity_vs_wl(nm).pdf", height=10, width=15)
#wl(um) vs Rth
plot(wl_um, R_th,
        xlab = expression("Wavelength"~"["*mu*m*"]"),
        ylab = "Theor. Responsivity [A/W]",
        pch = 16
)
dev.copy2pdf(file = "plots/photoconductivity/molfilm_theor_reponsivity_vs_wl(um).pdf", height=10, width=15)
#wl(nm) vs neqe
plot(wl, n_eqe,
        xlab = "Wavelength [nm]",
        ylab = expression(eta[E * Q * E]),
        pch = 16
)
dev.copy2pdf(file = "plots/photoconductivity/molfilm_neqe_vs_wl(nm).pdf", height=10, width=15)
#wl(um) vs neqe
plot(wl_um, n_eqe,
        xlab = expression("Wavelength"~"["*mu*m*"]"),
        ylab = expression(eta[E * Q * E]),
        pch = 16
)
dev.copy2pdf(file = "plots/photoconductivity/molfilm_neqe_vs_wl(um).pdf", height=10, width=15)