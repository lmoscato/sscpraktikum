Resitivity
file                          eff. Voltage range            Resitivity (ohm/mm)
AuAu -5 to 5V                       -4 to 0               8734019 +- 233884.6
AuAu 5 to -5V                       -4 to 0               8907207 +- 359432.3
AuAu -20 to 20V                     -16 to -5             19642331 +- 70678.7
BiAu -20 to 20V                     2.5 to 5.5            3008448 +- 175188.9
thinfilm -2 to 2 V pixel 1          1 to 1.48             11470822 +- 585972.2
thinfilm -2 to 2 V pixel 2          1 to 1.48             11592887 +- 487438.9
thinfilm -2 to 2 V pixel 3          0.92 to 1.3           10633747 +- 339420.6