rm(list=ls())

library(magicaxis)
library(httpgd)
source("fig_label.r")

par(mfrow = c(1,2), mar = c(3, 4, 2, 2) + 0.1)

data <- read.table("data/DFT/CsPbBr3_G5.bs.set-1.csv")
data_two <- read.table("data/DFT/CsPbBr3_G5_2.bs.set-1.csv")

x <- 1:length(data[,1])

valence_en <- max(data[4:71])
conduction_en <- min(data[72:91])
Eg <- conduction_en - valence_en

plot(x, data[,4],
    type = "l",
    ylim = c(-3, 8),
    xaxt = "n",
    yaxt = "n",
    ylab = "Energy [eV]",
    xlab = "",
    xaxs =  "i"
)

magaxis(2, hersh = TRUE)
axis(1, at = seq(1, length(data[,4]), length = 9), labels = c(expression(Gamma), "X", "S", "Y", "T","R", "U", "Z", expression(Gamma)))
text(40, mean(c(conduction_en, valence_en)), bquote(E[g]~ "="~ .(round(Eg, 2))~ eV))

for (i in 5:91) {
    color <- "black"
    if (i >= 72) {
       color <- "red"
    }

   lines(x, data[,i],
    col = color
   )
}

abline(h = c(valence_en, conduction_en), lty = 2, lwd = 1.5)

valence_en <- max(data_two[4:71])
conduction_en <- min(data_two[72:91])

Eg <- conduction_en - valence_en


plot(x, data_two[,4],
    type = "l",
    axes = FALSE,
    ylim = c(-3, 8),
    ylab = "",
    xlab = "",
    xaxs = "i"
)

box()
magaxis(2, hersh = TRUE)
axis(1, at = seq(1, length(data[,4]), length = 9), labels = c(expression(Gamma), "X", "S", "Y", "T","R", "U", "Z", expression(Gamma)))
text(40, mean(c(conduction_en, valence_en)), bquote(E[g]~ "="~ .(round(Eg, 2)) ~ eV))

for(i in 5:91){
    color <- "black"
    if(i >= 72){
        color <- "red"
    }
    
    lines(x, data_two[,i],
        col = color
    )
}


abline(h = c(valence_en, conduction_en), lty = 2, lwd = 1.5)
